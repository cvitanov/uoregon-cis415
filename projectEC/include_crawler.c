#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>
#include "linkedlist.h"
#include "p1fxns2.h" /* Borrowed from project 1 */
#define SIZE 1024 /* max line length in chars */
#define DEBUG 0
#define NCRIT 2
#define NUM_WORKERS 10

pthread_mutex_t mutex[NCRIT];

/* globals */
LinkedList *work_queue;
LinkedList *the_table;
LinkedList *directories;
int tokens[NCRIT] = { 0 };

typedef struct thread_args {
  int id;
} TARGS;

static pthread_t workers[NUM_WORKERS];

/* table entries for dictionary of files with dependencies */
struct table_entry {
  char* filename;
  LinkedList *deps;
};

typedef struct table_entry TE;

/* open file */

int open_file(char *fn, LinkedList *d) {
  int fd;
  int i,j,index;
  int length, length2;
  char the_file[SIZE];
  char * fptr;
  int len = d->get_size(d);
  for(i=0; i < len; i++) {
    fptr = d->remove_first(d);
    d->add_last(d,fptr);
    if(fptr == NULL) {
      length = 0;
    } else {
      length = p1strlen(fptr);
      for(j=0; j < length; j++)
        the_file[j] = fptr[j];
    }
    length2 = p1strlen(fn);
    if(DEBUG) printf("fn = %s, length2 = %d\n",fn,length2);
    index = 0;
    for(j=length; j < (length + length2); j++){
      the_file[j] = fn[index++];
    }
    the_file[j] = '\0';
    fptr = &the_file[0];
    if(DEBUG) printf("Trying to open %s\n",fptr);
    fd = open(fptr,O_RDONLY);
    if(DEBUG) printf("fd #%d returned from open\n",fd);
    if(fd != -1) {
      if(DEBUG) printf("1Open of %s successful\n",fptr);
      return fd;
    }
  }
  return -1;
}

/* insert a string in the middle of another string at index idx */
char* insert_string(char *str1, char *str2, int idx) {
  char *front = p1strdup(str1);
  char *back = p1strdup(&str1[idx]);
  front[idx] = '\0';
  p1strcat(front, str2);
  p1strcat(front, back);
  return front;
}

/* parse file */
void parse_file(char *fn, char **L) {
  char *p = p1strdup(fn);
  int len = p1strlen(p);
  int i;
  L[0] = &p[0];
  for(i = 0; i < len; i++) {
    if(p[i] == '.') {
      p[i] = '\0';
      L[1] = &p[i+1];
    }
  }
}

/* does str1 start with str2? */
int startswith(char *str1, char *str2) {
  int len = p1strlen(str2);
  int eq = p1strneq(str1, str2, len);
  return eq;
}

/* does str1 end with str2? */
int endswith(char *str1, char *str2) {
  int len1 = p1strlen(str1);
  int len2 = p1strlen(str2);
  int start = len1 - len2;
  int eq = p1strneq(&str1[start],str2,len2);
  return eq;
}

void ll_print_string(LinkedList *d) {
  int len = d->get_size(d);
  int i;
  char *c;
  for(i = 0; i < len; i++) {
    c = d->remove_first(d);
    if(DEBUG) printf("Entry #%d: %s\n",i,c);
    d->add_last(d,c);
  }
}

TE * table_lookup(char *fn, LinkedList *t) {
  int len = t->get_size(t);
  int str_len = p1strlen(fn);
  int i;
  TE *tableEntry = NULL;
  for(i = 0; i < len; i++) {
    tableEntry = t->remove_first(t);
    t->add_last(t,tableEntry);
    if(tableEntry == NULL)
      continue;
    if(p1strneq(fn,tableEntry->filename,str_len))
      return tableEntry;
  }
  return NULL;
}

/* returns 1 is matching string in string ll, otherwise 0 */
int ll_string_search(char *str, LinkedList *ll) {
  char * temp;
  int i;
  int len = ll->get_size(ll);
  int str_len = p1strlen(str);
  for(i = 0; i < len; i++) {
    temp = ll->remove_first(ll);
    ll->add_last(ll,temp);
    if(p1strneq(str,temp,str_len))
      return 1;
  }
  return 0;
}

/* search afile for lines of the form #include "xxx.h"
* for each one found, append the filename to `deps'
* if the filename not found in `table', insert it with empty dependency list
*    and append it to workq
*/
void process(TE *te, LinkedList *table, LinkedList *workq, LinkedList *d) {
  char buf[SIZE];
  char word[SIZE];
  char fixed[SIZE];
  char * fn;
  char * afile = te->filename;
  LinkedList * deps = te->deps;
  int idx;
  int len;
  int i;
  TE *tableEntry;
  int fd;
  fd = open_file(afile,d);
  if(fd == -1) {
    if(DEBUG) printf("Unable to open file: %s\n", afile);
    return;
  }

  while(p1getline(fd,buf,SIZE) != 0) {
    idx = p1getword(buf, 0, word);
    if((idx != -1) && p1strneq(word,"#include",8)) {
      /* found #include line */
      idx = p1getword(buf, idx, word);
      if(DEBUG) printf("Found #include %s for %s\n",word,afile);
      if((idx != -1) && word[0] == '"') {
        /* non-system include file */
        /* parse filename */
        len = p1strlen(word);
        fixed[0] = '\0';
        for(i = 1; i < len; i++) {
          if(word[i] == '"')
            break;
          fixed[i-1] = word[i];
        }
        fixed[i] = '\0';
        /* add to dependencies for afile */
        if(DEBUG) printf("Adding %s to dependency list for %s\n",&fixed[0],afile);
        /* add dependency to afile's dependency list */
        len = p1strlen(fixed);
        fn = (char *)malloc(sizeof(char) * ( len + 1 ));
        p1strcpy(fn,fixed);
        deps->add_last(deps,fn);
        if(DEBUG) printf("dep list for %s is now:\n",afile);
        if(DEBUG) ll_print_string(deps);
        if(DEBUG) printf("Looking for %s table\n",fn);




        /* acquire lock */
        pthread_mutex_lock(&mutex[1]);

        tableEntry = table_lookup(fn, table);

        /* release lock */
        pthread_mutex_unlock(&mutex[1]);

        if(tableEntry == NULL) {
          if(DEBUG) printf("Creating table entry for %s\n",afile);
          /* file not in table, add it and add to work queue */
          /* Table Entry for the file */
          tableEntry = (TE *)malloc(sizeof(TE));
          tableEntry->filename = fn;
          tableEntry->deps = LinkedList_create();

          /* acquire lock */
          pthread_mutex_lock(&mutex[1]);

          table->add_last(table,tableEntry);

          /* release lock */
          pthread_mutex_unlock(&mutex[1]);


          /* acquire lock */
          pthread_mutex_lock(&mutex[0]);

          workq->add_last(workq,fn);

          /* release lock */
          pthread_mutex_unlock(&mutex[0]);
        }

      }
    }
  }
  close(fd);
  return;
}

/*
*  iteratively print dependencies
*
* - use `printed' to guarantee that each file printed exactly once
* - use `to_process' to guarantee breadth-first order
*/

void print_dependencies(LinkedList *table, LinkedList *printed, LinkedList *to_process) {
  char * fn;
  TE *tableEntry;
  int i;
  int len;
  char * name;
  fn = to_process->remove_first(to_process);
  while(fn != NULL) {
    tableEntry = table_lookup(fn, table);
    len = tableEntry->deps->get_size(tableEntry->deps);
    if(DEBUG) printf("Table size for %s is %d\n",fn,len);
    for(i = 0; i < len; i++) {
      name = tableEntry->deps->remove_first(tableEntry->deps);
      tableEntry->deps->add_last(tableEntry->deps,name);
      if(ll_string_search(name,printed) == 0) {
        printf(" %s",name);
        printed->add_last(printed,name);
        to_process->add_last(to_process,name);
      }
    }
    fn = to_process->remove_first(to_process);
  }
  return;
}

void *worker_fxn(void *args) {
  TARGS *data = (TARGS *)args;
  char *afile;
  TE *tableEntry;

  if(DEBUG) printf("Thread %d waking up.\n",data->id);

  /* acquire lock */
  pthread_mutex_lock(&mutex[0]);

  afile = work_queue->remove_first(work_queue);

  /* release lock */
  pthread_mutex_unlock(&mutex[0]);

  while(afile != NULL) {
    if(DEBUG) printf("Processing file %s from workq\n",afile);

    /* acquire lock */
    pthread_mutex_lock(&mutex[1]);

    /* table_lookup function (Return pointer to table entry) */
    if(DEBUG) printf("Looking for %s in table\n",afile);
    tableEntry = table_lookup(afile, the_table);

    /* process function */
    if(DEBUG) printf("tableEntry->filename = %s\n",tableEntry->filename);

    /* release lock */
    pthread_mutex_unlock(&mutex[1]);

    process(tableEntry,the_table,work_queue,directories);

    /* acquire lock */
    pthread_mutex_lock(&mutex[0]);

    /* next file to process */
    afile = work_queue->remove_first(work_queue);

    /* release lock */
    pthread_mutex_unlock(&mutex[0]);
  }
}

int main(int argc, char *argv[]) {
  char *current = "./"; /* always search current directory first */
  int fstart = 1; /* index into argv for first filename */
  int i;
  char *cpath;
  int len;
  int idx;
  int thread_id;

  LinkedList *printed;
  LinkedList *to_process;

  char *obj;
  char *L[2];

  TE *tableEntry;

  static TARGS targs[NUM_WORKERS];

  /* using linked list for directory */
  directories = LinkedList_create();

  /* initialize mutex and cv vars */
  for (i = 0; i < NCRIT; i++) {
    pthread_mutex_init(&mutex[i],NULL);
  }

  /* acquire list of directories to search for include files */
  directories->add_last(directories,current);
  for(i = 1; i < argc; i++) {
    if( startswith(argv[i],"-I") ) { /* user specified another directory */
      argv[i] = &argv[i][2]; /* remove the '-I' tag */
      if( endswith(argv[i],"/") ) { /* make sure directory ends in '/' */
        directories->add_last(directories,argv[i]);
      } else {
        argv[i] = insert_string(argv[i],"/",p1strlen(argv[i]));
        directories->add_last(directories,argv[i]);
      }
    } else { /* all remaining arguments are filenames */
      fstart = i;
      break;
    }
  }
  /* obtain CPATH definition, if there */
  cpath = getenv("CPATH");
  if(cpath != NULL) {
    /* insert forward slashes whenever necessary */
    len = p1strlen(cpath);
    for(i = 0; i < len; i++) {
      if((i+1) < len && cpath[i] != '/' && cpath[i+1] == ':') {
        cpath = insert_string(cpath,"/",i+1);
        len++;
      }
    }
    if(cpath[len-1] != ':' && cpath[len-1] != '/') {
        p1strcat(cpath,"/");
        len++;
    }
    /* replace ':' with '\0' */
    /* then add each path to directory */
    idx = 0;
    for(i = 0; i < len; i++) {
      if(cpath[i] == ':') {
        cpath[i] = '\0';
        directories->add_last(directories,&cpath[idx]);
        idx = i + 1;
      }
    }
    directories->add_last(directories,&cpath[idx]);
  }

  ll_print_string(directories);


  /* populate the work queue */
  /* work queue linked list */
  work_queue = LinkedList_create();
  the_table = LinkedList_create();
  for(i = fstart; i < argc; i++) {
    parse_file(argv[i],L);
    if(DEBUG) printf("Argv[%d]: L[0] = %s, L[1] = %s\n",i,L[0],L[1]);
    len = p1strlen(L[1]);
    if(
      !(p1strneq(L[1],"c",len)) &&
      !(p1strneq(L[1],"l",len)) &&
      !(p1strneq(L[1],"y",len))
    ) {
      if(DEBUG) printf("Illegal argument: %s must end in .c, .l, or .y\n",argv[i]);
      goto cleanup;
    }

    /* Table Entry for obj file */
    obj = L[0];
    p1strcat(obj,".o");
    tableEntry = (TE *)malloc(sizeof(TE));
    tableEntry->filename = obj;
    tableEntry->deps = LinkedList_create();
    tableEntry->deps->add_last(tableEntry->deps,argv[i]);
    the_table->add_last(the_table,tableEntry);

    /* add file to work queue */
    work_queue->add_last(work_queue,argv[i]);

    /* Table Entry for the file */
    tableEntry = (TE *)malloc(sizeof(TE));
    tableEntry->filename = argv[i];
    tableEntry->deps = LinkedList_create();
    the_table->add_last(the_table,tableEntry);
  }

  /* at this point, we have all the .o's, .c's, .l's, and .y's in the_table */
  /* process the work queue */
  for(i=0; i < NUM_WORKERS; i++) {
    thread_id = i;
    targs->id = thread_id;
    pthread_create(&workers[i],NULL,worker_fxn,&targs[i]);
    if(DEBUG) printf("Created thread %d\n",i);
  }

  /* wait on worker threads */
  for(i = 0; i < NUM_WORKERS; i++)
   pthread_join(workers[i], NULL);

  /* at this point, the table contains the full dependencies for all files. */
  /* harvest data and print results */
  for(i=fstart; i<argc; i++) {
    parse_file(argv[i],L);
    obj = L[0];
    p1strcat(obj,".o");
    printf("%s:",obj);
    printed = LinkedList_create();
    printed->add_last(printed,obj);
    to_process = LinkedList_create();
    to_process->add_last(to_process,obj);
    print_dependencies(the_table,printed,to_process);
    printf("\n");
    /* destroy ll data for printed and to_process */
    LinkedList_destroy(printed);
    printed = NULL;
    LinkedList_destroy(to_process);
  }

  goto cleanup;
  /* clean up */
  cleanup:
    if(directories != NULL)
      LinkedList_destroy(directories);
    if(work_queue != NULL)
      LinkedList_destroy(work_queue);
    if(the_table != NULL) {
      /* cleanup table entry linked lists */

      len = the_table->get_size(the_table);
      for(i = 0; i < len; i++) {
        tableEntry = the_table->remove_first(the_table);
        LinkedList_destroy(tableEntry->deps);
        free(tableEntry);
        tableEntry = NULL;
      }
      LinkedList_destroy(the_table);
    }
    return 0;
}

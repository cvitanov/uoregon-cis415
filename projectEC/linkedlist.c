#include "linkedlist.h"
#include <stdlib.h>
#include <stdio.h>

/* Linked List Node */
struct ll_node {
  void *data;
  struct ll_node *nextn;
  struct ll_node *prevn;
};

typedef struct ll_node LLNode;

/* linked list data */
typedef struct ll_data {
  int size;
  int current_idx;
  LLNode* current;
  LLNode* sentinel;
} LLData;

/* relink_  (links node between before and after) */
static void relink_(LLNode* before, LLNode* node, LLNode* after) {
  node->nextn = after;
  node->prevn = before;
  after->prevn = node;
  before->nextn = node;
}

/* unlink_ (unlinks node from doubly linked list) */
static void unlink_(LLNode* node) {
  node->prevn->nextn = node->nextn;
  node->nextn->prevn = node->prevn;
  free(node);
  node = NULL;
}

/* returns lenth of linked list */
static int len(LinkedList* ll) {
  LLData *lld = (LLData *)ll->self;
  return lld->size;
}

/* add new node with data to the linked list */
static void ll_add_last(LinkedList *ll, void *data) {
  LLNode* node;
  LLData *lld = (LLData *)ll->self;

  node = (LLNode *)malloc(sizeof(LLNode));
  node->data = data;
  relink_(lld->sentinel->prevn,node,lld->sentinel);
  lld->size += 1;
}

/* remove first node's data from linked list */
static void *ll_remove_first(LinkedList *ll) {
  void *data = NULL;
  LLData *lld = (LLData *)ll->self;

  if (len(ll) <= 0)
    return data;
  data = lld->sentinel->nextn->data;
  unlink_(lld->sentinel->nextn);
  lld->size -= 1;

  return data;
}

/* get size */
static int ll_get_size(LinkedList *ll) {
  int l;
  LLData *lld = (LLData *)ll->self;
  l = len(ll);
  return l;
}

/* linked list template */
static LinkedList template = {
  NULL, ll_add_last, ll_remove_first, ll_get_size
};

/* create new linked list (empty) */
LinkedList *LinkedList_create() {
  LinkedList *newLL = (LinkedList *)malloc(sizeof(LinkedList));
  if (newLL != NULL) {
    LLData *lld = (LLData *)malloc(sizeof(LLData));
    lld->current = NULL;
    if (lld != NULL) {
      lld->sentinel = (LLNode *)malloc(sizeof(LLNode));
      if(lld->sentinel != NULL) {
        lld->size = 0;
        lld->sentinel->nextn = lld->sentinel;
        lld->sentinel->prevn = lld->sentinel;
        *newLL = template;
        newLL->self = lld;
      }
      else {
        free(lld);
        lld = NULL;
        free(newLL);
        newLL = NULL;
      }
    }
    else {
      free(newLL);
      newLL = NULL;
    }
  }
  return newLL;
}

/* destroy linked list */
void LinkedList_destroy(LinkedList *ll) {
  void *data;
  LLData *lld = (LLData *)ll->self;
  while(len(ll) > 0) {
    data = ll_remove_first(ll);
  }
  free(lld->sentinel);
  lld->sentinel = NULL;
  free(lld);
  lld = NULL;
  free((void *)ll);
  ll = NULL;
}

#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_


typedef struct linked_list LinkedList;

/* create new linked list (empty) */
LinkedList *LinkedList_create();

/* destroy linked list */
void LinkedList_destroy(LinkedList *ll);

struct linked_list {
  void *self; /* instance-specfific data structure */
  /* add data to the end of the linked list */
  void (*add_last)(LinkedList *LL, void *data);
  /* remove data from the front of the linked list */
  void* (*remove_first)(LinkedList *LL);
  /* get size of linked list */
  int (*get_size)(LinkedList *LL);
};

#endif /* _LINKEDLIST_H_ */

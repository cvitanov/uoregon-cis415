#include "networkdriver.h"
#include "packetdescriptor.h"
#include "BoundedBuffer.h"
#include "diagnostics.h"
#include "freepacketdescriptorstore__full.h"
#include "pid.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <unistd.h>

#define RECV_SIZE 1
#define SEND_ATTEMPTS 2


/* arguments for thread functions */
typedef struct thread_args {
  BoundedBuffer *pool;
  NetworkDevice *nd;
  FreePacketDescriptorStore *fpds;
} TARGS;

/* globals */
static BoundedBuffer *send;
static BoundedBuffer *recv[MAX_PID+1];
static FreePacketDescriptorStore *fpds;

/* helper functions */

/* put pd in pool or fpds if that fails, else log error */
/* return 1 if successful, 0 if not */
static int nb_put_pd (BoundedBuffer *pool,FreePacketDescriptorStore *fpds, PacketDescriptor *pd)
{
  if (pool->nonblockingWrite(pool,pd) == 0)
    if (fpds->nonblockingPut(fpds,pd) == 0)
      return 0;
  return 1;
}

/* get pd */
/* return 1 if successful, 0 if not */
static int nb_get_pd (BoundedBuffer *pool,FreePacketDescriptorStore *fpds, PacketDescriptor *pd)
{
  if (pool->nonblockingRead(pool,(void **)&pd) == 0)
    if (fpds->nonblockingGet(fpds,&pd) == 0)
      return 0;
  return 1;
}

/* sender thread function */
static void *sender (void *args)
{
  int i;
  PacketDescriptor *pd;
  TARGS *data = (TARGS *)args;
  int thePID;

  while(1)
  {
    /* grab pd from send bb */
    send->blockingRead(send,(void **)&pd);
    /* try to send SEND_ATTEMPTS times */
    for (i = 0; i < SEND_ATTEMPTS; i++)
    {
      /* try to send */
      if( (data->nd->sendPacket(data->nd,pd)) ) {
        thePID = (int)getPID(pd);
        DIAGNOSTICS("Driver: Successfully sent packet for App#%d after %d attempts.\n",thePID,(i+1));
        break;
      }

      /* failure, wait 100ms */
      usleep(1000000);
    }
    /* return packet to pool, or fpds if that fails */
    if(nb_put_pd(data->pool,data->fpds,pd) == 0)
      DIAGNOSTICS("Driver: Failed to return pd after send!\n");
  }
  return NULL;
}

/* receiver thread function */
static void *receiver (void *args) {
  PacketDescriptor *pd;
  BoundedBuffer *bb;
  TARGS *data = (TARGS *)args;
  int thePID;

  /* initialize network device to receive packets right away */
  /* wait on first available packet to use */
  data->fpds->blockingGet(fpds,&pd);
  initPD(pd);
  data->nd->registerPD(data->nd,pd);
  /* wait on packet */
  data->nd->awaitIncomingPacket(data->nd);

  /* try to add packet to recv bb */
  /* determine PID for the pd */
  thePID = (int)getPID(pd);
  bb = recv[thePID];
  if( !(bb->nonblockingWrite(bb,pd)) )
  {
    /* buffer full, discard packet in pool or fpds if that fails */
    if(!(nb_put_pd(data->pool,data->fpds,pd)))
      DIAGNOSTICS("Driver: Failed to return pd after send!\n");
  }

  /* now loop forever and collect additional packets for dispatch to apps */
  /* packets are stored in the corresponding buffer for each app's pid */
  /* do not block on available packets in pool or fpds */
  while(1) {
    /* get empty pd ready */
    if(nb_get_pd(data->pool,data->fpds,pd) == 0)
    {
      DIAGNOSTICS("Driver: Failed to find free pd for receive!\n");
    }
    else
    {
      /* initialize pd */
      initPD(pd);
      /* register new empty PD */
      data->nd->registerPD(data->nd,pd);
      /* wait on packet */
      data->nd->awaitIncomingPacket(data->nd);


      /* determine PID for the pd */
      thePID = (int)getPID(pd);
      DIAGNOSTICS("Driver: Packet for App #%d received from Network.\n",thePID);

      /* try to add packet to recv bb */
      bb = recv[thePID];
      if( !(bb->nonblockingWrite(bb,pd)) )
      {
        /* buffer full, discard packet in pool or fpds if that fails */
        if(!(nb_put_pd(data->pool,data->fpds,pd)))
          DIAGNOSTICS("Driver: Failed to return pd after send!\n");
      }
    }
  }
  return NULL;
}

/* Required Procedures */

void init_network_driver(NetworkDevice               *nd,
                         void                        *mem_start,
                         unsigned long               mem_length,
                         FreePacketDescriptorStore **fpds_ptr)
{

  int i;
  PacketDescriptor *pd;
  static TARGS args; /* struct with arguments for threads */
  BoundedBuffer *pool;
  pthread_t sthread, rthread;
  int fpds_size, pool_size, send_size;

  /* store pointer to network device */
  args.nd = nd;

  /* create FPDS using mem_start, mem_length */
  fpds = FreePacketDescriptorStore_create(mem_start, mem_length);
  *fpds_ptr = fpds;
  args.fpds = fpds;
  /* get size of fpds */
  fpds_size = (int)fpds->size(fpds);
  /* calculate a good size for send buffer and pool buffers */
  if(MAX_PID < (fpds_size / 2))
  {
    pool_size = (int)(fpds_size / 2);
    send_size = pool_size;
  }
  else
  {
    pool_size = MAX_PID;
    send_size = MAX_PID;
  }

  /* create buffers: */

  /* one "outgoing" buffer */
  send = BoundedBuffer_create(send_size);

  /* one "pool" buffer */
  /* initialize pool with packets from fpds */
  pool = BoundedBuffer_create(pool_size);
  for(i = 0; i < pool_size; i++)
  {
    fpds->blockingGet(fpds,&pd);
    pool->blockingWrite(pool,pd);
  }
  args.pool = pool;

  /* receiver buffers (one singleton buffer for each app)*/
  for(i = 0; i < (MAX_PID+1); i++)
    recv[i] = BoundedBuffer_create(RECV_SIZE);

  /* create one send thread to handle sending PD's to network device */
  pthread_create(&sthread,NULL,sender,&args);
  /* create one receiver thread to handle pd transfer to apps */
  pthread_create(&rthread,NULL,receiver,&args);
  /* return FPDS to caller (nothing to do?)*/
}

void blocking_send_packet(PacketDescriptor *pd)
{
  send->blockingWrite(send,pd);
}

int nonblocking_send_packet(PacketDescriptor *pd)
{
  return send->nonblockingWrite(send,pd);
}

void blocking_get_packet(PacketDescriptor **pd, PID pid)
{
  int thePID = (int)pid;
  BoundedBuffer *bb = recv[thePID];
  bb->blockingRead(bb,(void **)pd);
}

int  nonblocking_get_packet(PacketDescriptor **pd, PID pid)
{
  int thePID = (int)pid;
  BoundedBuffer *bb = recv[thePID];
  return ( bb->nonblockingRead(bb,(void **)pd) );
}

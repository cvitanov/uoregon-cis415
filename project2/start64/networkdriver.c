/*
 *
 * AUTHOR: Andrew Cvitanovich
 * DUCKID: cvitanov
 * TITLE: CIS 415 Project 2
 *
 * THIS IS MY WORK.
 *
 */

#include "networkdriver.h"
#include "packetdescriptor.h"
#include "BoundedBuffer.h"
#include "diagnostics.h"
#include "freepacketdescriptorstore__full.h"
#include "pid.h"
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#define RECV_SIZE 1
#define SEND_ATTEMPTS 2
#define RECV_THREAD_POOL_SIZE 5
#define SEND_THREAD_POOL_SIZE 5

/* token to identify current thread */
volatile int recv_token = 0;
volatile int send_token = 0;

/* arguments for thread functions */
enum t_type{RECV, SEND};
typedef struct thread_args {
  NetworkDevice *nd;
  int id;
  enum t_type thread_type;
} TARGS;

/* globals */
static BoundedBuffer *cache;
static BoundedBuffer *send;
static BoundedBuffer *recv[MAX_PID+1];
static FreePacketDescriptorStore *fpds;

/* mutex for critical section (network device receive) */
static pthread_mutex_t recv_mutex = PTHREAD_MUTEX_INITIALIZER;
/* mutex for critical section (network device send) */
static pthread_mutex_t send_mutex = PTHREAD_MUTEX_INITIALIZER;

/* condition variable for receiving packets from network device */
static pthread_cond_t recv_cv = PTHREAD_COND_INITIALIZER;
/* condition variable for sending packets from network device */
static pthread_cond_t send_cv = PTHREAD_COND_INITIALIZER;

static pthread_t recv_threads[RECV_THREAD_POOL_SIZE];
static pthread_t send_threads[SEND_THREAD_POOL_SIZE];

/* helper functions */

/* put pd in cache or fpds if that fails, else log error */
/* return 1 if successful, 0 if not */
static int nb_put_pd (BoundedBuffer *cache,FreePacketDescriptorStore *fpds, PacketDescriptor *pd)
{
  if (cache->nonblockingWrite(cache,pd) == 0)
    if (fpds->nonblockingPut(fpds,pd) == 0)
      return 0;
  return 1;
}

/* get pd */
/* return 1 if successful, 0 if not */
static int nb_get_pd (BoundedBuffer *pool,FreePacketDescriptorStore *fpds, PacketDescriptor **pd)
{
  if (pool->nonblockingRead(pool,(void **)pd) == 0)
    if (fpds->nonblockingGet(fpds,pd) == 0)
      return 0;
  return 1;
}

/* receiver thread fxn */
static void *receiver (void *args)
{
  PacketDescriptor *pd;
  BoundedBuffer *bb;
  TARGS *data = (TARGS *)args;
  int thePID;

  /* setup steps for zeroth thread */
  if(data->id == 0)
  {
    /* initialize network device to receive packets right away */
    /* wait on first available packet to use */
    fpds->blockingGet(fpds,&pd);
    initPD(pd);
    data->nd->registerPD(data->nd,pd);
    /* wait on packet */
    data->nd->awaitIncomingPacket(data->nd);

    /* try to add packet to recv bb */
    /* determine PID for the pd */
    thePID = (int)getPID(pd);
    bb = recv[thePID];
    if( !(bb->nonblockingWrite(bb,pd)) )
    {
      /* buffer full, discard packet in pool or fpds if that fails */
      if(!(nb_put_pd(cache,fpds,pd)))
        DIAGNOSTICS("Driver: Failed to return pd after send! (thread %d)\n",data->id);
    }

  }

  /* now loop forever and collect additional packets for dispatch to apps */
  /* packets are stored in the corresponding buffer for each app's pid */
  /* do not block on available packets in pool or fpds */
  while(1) {
    /* get empty pd ready */
    if(nb_get_pd(cache,fpds,&pd) == 0)
    {
      DIAGNOSTICS("Driver: Failed to find free pd for receive! (thread %d)\n",data->id);
    }
    else
    {

      /* sleep and retry if this thread did not get a pd it can use */
      /* weird bug where nb_get_pd claims success but hands thread a null pointer */
      if(pd == NULL)
      {
          usleep(100000);
          continue;
      }

      /* initialize pd */
      initPD(pd);

      /* acquire lock */
      pthread_mutex_lock(&recv_mutex);

      /* wait for the condition variable to be signaled */
      while (recv_token != data->id)
        pthread_cond_wait(&recv_cv,&recv_mutex);

      /* register new empty PD */
      data->nd->registerPD(data->nd,pd);
      /* wait on packet */
      data->nd->awaitIncomingPacket(data->nd);

      recv_token = (recv_token+1) % RECV_THREAD_POOL_SIZE;

      /* broadcast all threads waiting on signal */
      pthread_cond_broadcast(&recv_cv);

      /* release the lock */
      pthread_mutex_unlock(&recv_mutex);

      /* determine PID for the pd */
      thePID = (int)getPID(pd);
      DIAGNOSTICS(
        "Driver: Packet for PID %d received from network by thread %d.\n",
        thePID,data->id);

      /* try to add packet to recv bb */
      bb = recv[thePID];
      if( !(bb->nonblockingWrite(bb,pd)) )
      {
        /* buffer full, discard packet in pool or fpds if that fails */
        if(!(nb_put_pd(cache,fpds,pd))) {
          DIAGNOSTICS("Driver: Failed to discard pd! (recv_thread %d)",data->id);
          fpds->blockingPut(fpds,pd);
        }
      }
    }
  }
  return NULL;
}

/* sender thread fxn */
static void *sender (void *args)
{
  PacketDescriptor *pd;
  TARGS *data = (TARGS *)args;
  int thePID, i, success;

  while(1)
  {
    /* send msg to network device */

    /* grab pd from send bb */
    send->blockingRead(send,(void **)&pd);

    /* need to protect critical section here: only one thread can wait on
     * incoming packet at a time */

    /* try to send SEND_ATTEMPTS times */
    for (i = 0; i < SEND_ATTEMPTS; i++)
    {
      /* acquire lock */
      pthread_mutex_lock(&send_mutex);

      /* wait for the condition variable to be signaled */
      while (send_token != data->id)
        pthread_cond_wait(&send_cv,&send_mutex);

      success = data->nd->sendPacket(data->nd,pd);

      send_token = (send_token+1) % SEND_THREAD_POOL_SIZE;

      /* broadcast all threads waiting on signal */
      pthread_cond_broadcast(&send_cv);

      /* release the lock */
      pthread_mutex_unlock(&send_mutex);

      /* try to send */
      if(!success) {
        thePID = (int)getPID(pd);
        DIAGNOSTICS(
          "Driver: Successfully sent packet for App#%d after %d attempts. (thread %d)\n",
          thePID,(i+1),data->id);
        break;
      }

      /* failure, wait 100ms */
      usleep(1000000);
    }


    /* return packet to pool, or fpds if that fails */
    if(nb_put_pd(cache,fpds,pd) == 0) {
      DIAGNOSTICS("Driver: Failed to return pd after send! (thread %d)\n",data->id);
      fpds->blockingPut(fpds,pd);
    }
  }
  return NULL;
}

/* Required Procedures */

void init_network_driver(NetworkDevice               *nd,
                         void                        *mem_start,
                         unsigned long               mem_length,
                         FreePacketDescriptorStore **fpds_ptr)
{

  int i;
  PacketDescriptor *pd;
  int fpds_size, cache_size, send_size;
  static TARGS recv_args[RECV_THREAD_POOL_SIZE]; /* structs with arguments for receiver threads */
  static TARGS send_args[SEND_THREAD_POOL_SIZE]; /* structs with arguments for sender threads */

  /* create FPDS using mem_start, mem_length */
  fpds = FreePacketDescriptorStore_create(mem_start, mem_length);
  *fpds_ptr = fpds;

  /* get size of fpds */
  fpds_size = (int)fpds->size(fpds);
  /* calculate a good size for send buffer and cache buffers */
  cache_size = (int)((fpds_size - MAX_PID - 1) / 2);
  if(cache_size > 0) {
    send_size = cache_size;
  }
  else {
    cache_size = 1;
    send_size = 1;
  }

  /* create buffers: */

  /* one "outgoing" buffer */
  send = BoundedBuffer_create(send_size);

  /* one "cache" buffer */
  /* initialize cache with packets from fpds */
  cache = BoundedBuffer_create(cache_size);
  for(i = 0; i < cache_size; i++)
  {
    fpds->blockingGet(fpds,&pd);
    cache->blockingWrite(cache,pd);
  }

  /* receiver buffers (one singleton buffer for each app)*/
  for(i = 0; i < (MAX_PID+1); i++)
    recv[i] = BoundedBuffer_create(RECV_SIZE);

  /* create threads for receiving packets */
  for (i = 0; i < RECV_THREAD_POOL_SIZE; i++)
  {
    recv_args[i].id = i;
    recv_args[i].nd = nd;
    recv_args[i].thread_type = RECV;
    pthread_create(&recv_threads[i],NULL,receiver,&recv_args[i]);
  }

  /* create threads for sending packets */
  for (i = 0; i < SEND_THREAD_POOL_SIZE; i++)
  {
    send_args[i].id = i;
    send_args[i].nd = nd;
    send_args[i].thread_type = SEND;
    pthread_create(&send_threads[i],NULL,sender,&send_args[i]);
  }

}

void blocking_send_packet(PacketDescriptor *pd)
{
  send->blockingWrite(send,pd);
}

int nonblocking_send_packet(PacketDescriptor *pd)
{
  return send->nonblockingWrite(send,pd);
}

void blocking_get_packet(PacketDescriptor **pd, PID pid)
{
  int thePID = (int)pid;
  BoundedBuffer *bb = recv[thePID];
  bb->blockingRead(bb,(void **)pd);
}

int  nonblocking_get_packet(PacketDescriptor **pd, PID pid)
{
  int thePID = (int)pid;
  BoundedBuffer *bb = recv[thePID];
  return ( bb->nonblockingRead(bb,(void **)pd) );
}

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>

int main()
{
    pid_t pid = fork();
    int status;
    int result;
    if (pid == 0) {
        printf("CHILD\n");
        char* argv[1024];
        char s[9] = "ls\0/proc\0";
        argv[0] = &s[0];
        argv[1] = &s[3];
        argv[2] = NULL;
        execvp(argv[0],argv);
	printf("EXITING...\n");
        exit(0);
        printf("Hello, World!\n");
        exit(0);
    } else {
        printf("PARENT\n");
        int result = waitpid(pid,&status,0);
        printf("status %d pid %d result %d\n", status, (int) pid, result);
    	exit(0);
    }
    return 0;
}

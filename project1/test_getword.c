#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
static char *singlequote = "'";
static char *doublequote = "\"";
static char *whitespace = " \t";

int p1strlen(char *s) {
    char *p = s;

    while (*p != '\0')
        p++;
    return (p - s);
}

int p1strchr(char buf[], char c) {
    int i;

    for (i = 0; buf[i] != '\0'; i++)
        if (buf[i] == c)
            return i;
    return -1;
}
int p1getword(char buf[], int i, char word[]) {
    char *tc, *p;

    /* skip leading white space */
    while(p1strchr(whitespace, buf[i]) != -1)
        i++;
    /* buf[i] is now '\0' or a non-blank character */
    if (buf[i] == '\0')
        return -1;
    p = word;
    switch(buf[i]) {
    case '\'': tc = singlequote; i++; break;
    case '"': tc = doublequote; i++; break;
    default: tc = whitespace; break;
    }
    while (buf[i] != '\0') {
        if (p1strchr(tc, buf[i]) != -1)
            break;
        *p++ = buf[i];
        i++;
    }
    /* either at end of string or have found one of the terminators */
    if (buf[i] != '\0') {
        if (tc != whitespace) {
            i++;	/* skip over terminator */
        }
    }
    *p = '\0';
    return i;
}
char *p1strdup(char *s) {
    int n = p1strlen(s) + 1;
    char *p = (char *)malloc(n);
    int i;

    if (p != NULL) {
        for (i = 0; i < n; i++)
            p[i] = s[i];
    }
    return p;
}
int main() {
  int idx;
  char word[1024];
  char buf[] = "ls ~";

  idx = 0;
  printf("%s\n",buf);

  for(;;) {
    idx = p1getword(buf, idx, &word[idx]);

    word[idx++] = ':';
    printf("idx=%d, word=%s\n",idx,word);
    if(0 >= idx) break;
  }
  printf("%s\n",word);
  return 0;
}

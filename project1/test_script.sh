#!/bin/bash

unset USPS_QUANTUM_MSEC
echo "Set USPS_QUANTUM_MSEC to " $USPS_QUANTUM_MSEC
echo "Testing Script"

echo "This should exit, no quantum."
./uspsv1

echo "This should exit, bad args."
./uspsv1 --quaint=30 b.txt

echo "This should exit, bad args."
./uspsv1 bad args

echo "This should exit, bad quantum"
./uspsv1 --quantum=BAD workload.txt

echo "This should exit, no quantum."
./uspsv1 <workload.txt

echo "This should exit, no quantum."
./uspsv1 workload.txt


echo "This should read from stdin, (piping workload file into stdin)"
./uspsv1 --quantum=25 <workload.txt

echo "This should read from file"
./uspsv1 --quantum=25 workload.txt

export USPS_QUANTUM_MSEC=50
echo "Set USPS_QUANTUM_MSEC to " $USPS_QUANTUM_MSEC

echo "This should work, env set. Read from stdin."
./uspsv1 <workload.txt

echo "This should work, env set, read from file."
./uspsv1 workload.txt

echo "This should work, overriding env var,(piping workload file into stdin)"
./uspsv1 --quantum=25 <workload.txt

echo "This should work, overriding env var, read from file"
./uspsv1 --quantum=25 workload.txt

echo "This should work."
./uspsv2 workload.txt

echo "This should work."
./uspsv2 <workload.txt

echo "This should work, override long quantum, some programs exit before first cycle"
./uspsv2 --quantum=2500 workload.txt

echo "This should work."
./uspsv3 workload.txt

echo "This should work, override long quantum, some programs exit before first cycle"
./uspsv3 --quantum=2500 workload.txt

echo "This should work."
./uspsv3 <workload.txt

echo "This should work."
./uspsv4 workload.txt

echo "This should work, override long quantum, some programs exit before first cycle"
./uspsv4 --quantum=2500 workload.txt


echo "This should work."
./uspsv4 <workload.txt

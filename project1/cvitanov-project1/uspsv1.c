/*
 *
 * AUTHOR: Andrew Cvitanovich
 * DUCKID: cvitanov
 * TITLE: CIS 415 Project 1
 *
 * THIS IS MY WORK.
 *
 */
#include <stdio.h>
#define _POSIX_SOURCE
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/times.h>
#include "p1fxns.h"
// #define DEBUG 0
#define QUANTUM_LEAP 1000 // limit for quantum usec
#define SIZE 1024 // size for array storage
#define PID_LIMIT 32768 // max number of processes

// write string to stdout
int pstdout(char *str) {
    write(STDOUT_FILENO, str, sizeof(char) * p1strlen(str));

    return 0;
}

// fill array with char ch
void clearArray(char buf[], int sz, char ch) {
  for(int i = 0; i < sz; i++) {
    buf[i] = ch;
  }
}

// append word to end of program arguments string buffer
int appendWord(char prog[], int idx, char word[]) {
  int i = 0;
  while(word[i] != '\0')
    prog[idx++] = word[i++];
  prog[idx++] = '\0';
  return idx;
}

// remove any empty arguments (parsing artifacts)
void removeEmptyArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++) {
    if ( args[i] == NULL )
      break;
    if ( p1strlen(args[i]) == 0  )
      args[i] = NULL;
  }
}

// clear list of arguments
void clearArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++)
    args[i] = NULL;
}

// get the quantum from environment
int getQuantum(void) {
  int val;
  char * s = getenv("USPS_QUANTUM_MSEC");
  (s != NULL) ? (val = p1atoi(s)) : (val = -1);
  return val;
}

// try to set the quantum value with argument
// if the argument is invalid return -1
int tryToSetQuantum(char arg[]) {
  char q[] = "--quantum=";
  int i = 0;
  char val_str[5];
  int val = 0;
  int cnt = 0;

  // check if valid argument
  while(i < 10) {
    if (q[i] != arg[i] || arg[i] == '\0')
      return -1; // not a valid argument for setting quantum
    i++;
  }

  // check if valid value
  while(arg[i] != '\0' && cnt < 4) {
    if ( arg[i] < '0' || arg[i] > '9') {
      return -1; // invalid value for quantum
    } else {
      val_str[cnt++] = arg[i++];
    }
  }
  val = p1atoi(val_str);

  // return default if value exceeds max quantum limit
  if (val >= QUANTUM_LEAP)
    return getQuantum();

  // return value in argument
  return val;
}

int main(int argc, char *argv[] ) {
  pid_t pids[PID_LIMIT]; /* TODO: Allow unlimited number of pids */
  int fd, status, quantum;
  char program[SIZE];
  char buf[SIZE];
  char word[SIZE];
  char * args[SIZE]; /* pointer array for program args */
  char message[SIZE];
  char pidno[64];

  // hangle arguments
	if (argc == 1) {

    // fail if quantum not set
    if ( (quantum = getQuantum()) == -1 )
      return -1;

    /* READ FROM STDIN */
    fd = 0;

  } else if (argc == 2) {

    // try to set quantum
    quantum = tryToSetQuantum(argv[1]);

    if(quantum == -1) {

      // fail if quantum not set
      if ( (quantum = getQuantum()) == -1 )
        return -1;

      // try to read file
      fd = open(argv[1], O_RDONLY | O_EXCL);
      if(fd < 0) {
        p1strcpy(message,"Error opening ");
        p1strcat(message,argv[1]);
        p1perror(fd,message);
      }
    } else {

      /* READ FROM STDIN */
      fd = 0;

    }

  } else if (argc == 3) {

    // try to set quantum
    quantum = tryToSetQuantum(argv[1]);

    // bad quantum argument
    if( quantum == -1 )
      return -1;

    // try to read file
    fd = open(argv[2], O_RDONLY | O_EXCL);
    if(fd < 0) {
      p1strcpy(message,"Error opening ");
      p1strcat(message,argv[1]);
      p1perror(fd,message);
    }

  } else {

    // TOO MANY ARGS!
    p1strcpy(message,"Too many arguments!\n");
    pstdout(message);
    return -1;

  }

  if(fd < 0)
    return -1;

  /* LOOP THROUGH PROGRAM LIST */
  int idx_program = 0;
  while( p1getline(fd, buf, SIZE) != 0 ) {

    // init parameters
    clearArgs(args,SIZE);
    clearArray(program, SIZE, '\0');
    int j = 0;
    int cnt = 0;
    int idx = 0;
    int argc = 0;

    // parse program name and arguments
    for (;;) {

      // get next word
      idx = p1getword(buf, idx, word);

      if (idx < 0)
        break;

      // construct array of argument pointers using this word
      args[cnt++] = &program[j];
      j = appendWord(program, j, word);
      argc++;

    }

    // set last argument to NULL
    args[cnt] = NULL;

    // remove newlines for program string
    for(int i = 0; i < SIZE; i++) {
      if(program[i] == '\n')
        program[i] = '\0';
    }

    // remove empty args
    removeEmptyArgs(args, SIZE);

    // skip empty programs
    if(args[0] == NULL)
      continue;


    // FORK, IF CHILD then EXECVP and EXIT
    if ( (pids[idx_program] = fork()) == -1 )
      perror("fork() error");

    // if this is child process
    // then execute program then exit
    if (pids[idx_program] == 0) {
      //
      // if(DEBUG) {
      //   p1strcpy(message,"Executing child with pid ");
      //   p1itoa((int) getpid(),pidno);
      //   p1strcat(message,pidno);
      //   p1strcat(message,"\n");
      //   pstdout(message);
      // }


      // try to execute program
      execvp(args[0],args);

      p1strcpy(message,"Error executing in child with pid ");
      p1itoa((int) getpid(),pidno);
      p1strcat(message,pidno);
      perror(message);
      exit(errno);
    }

    // increment program count
    idx_program++;

  }
  // if(DEBUG) {printf("QUANTUM = %d\n", quantum);}


  // wait for programs to finish
  for(int i = 0; i < idx_program; i++)
    (void) waitpid(pids[i],&status,0);

  exit(0);
}

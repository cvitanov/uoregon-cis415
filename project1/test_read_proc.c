#include<stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <sys/times.h>
#include <unistd.h>
#include "p1fxns.h"

#define SIZE 1024
int main() {

    pid_t pid;
    int fd;
    int idx = 0;
    int j = 0;
    char buf[SIZE];
    char word[SIZE];
    char output[SIZE];
    char fname[100];
    char value[10];

    //char newlines[] = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    //write(1, newlines, sizeof(newlines));
    char clr[] = "\e[2J\e[H";
    write(1,clr,sizeof(clr));

    // print a string for top column (stdout)
    p1putstr(1, "NAME\t\tSTATE\tPID\tMemsize(kB)\tRead(bytes)\tWrite(bytes)\tUserTime\n");

    for(int k = 0; k < sz; k++) {


      // using pid
      pid = pids[k];

      // open /proc/status for reading
      p1strcpy(fname,"/proc/");
      p1itoa((int)pid, value);
      p1strcat(fname, value);
      p1strcat(fname, "/status");

      fd = open(fname, O_RDONLY | O_EXCL);

      // read first line and get program name, then append to output
      p1getline(fd, buf, SIZE);
      idx = p1getword(buf,0,word);
      idx = p1getword(buf,idx,word); // second word
      p1strcpy(output,"");
      if( (j = p1strchr(word,'\n')) != -1 )
      word[j] = '\0';
      p1strcat(output,word);
      p1strcat(output,"\t\t");

      // read second line, state
      p1getline(fd, buf, SIZE);
      idx = p1getword(buf,0,word);
      idx = p1getword(buf,idx,word); // second word
      if((j = p1strchr(word,'\n')) != -1)
      word[j] = '\0';
      p1strcat(output,word);
      p1strcat(output,"\t");

      // read 5th line, pid
      p1getline(fd, buf, SIZE);
      p1getline(fd, buf, SIZE);
      p1getline(fd, buf, SIZE);
      idx = p1getword(buf,0,word);
      idx = p1getword(buf,idx,word); // second word
      if((j = p1strchr(word,'\n')) != -1)
      word[j] = '\0';
      p1strcat(output,word);
      p1strcat(output,"\t");

      // read 12th line, Memsize (virtual)
      for(int j = 0; j < 7; j++)
      p1getline(fd, buf, SIZE);
      idx = p1getword(buf,0,word);
      idx = p1getword(buf,idx,word); // second word
      if((j = p1strchr(word,'\n')) != -1)
      word[j] = '\0';
      p1strcat(output,word);
      p1strcat(output,"\t\t");

      // open /proc/io for reading
      p1strcpy(fname,"/proc/");
      p1itoa((int)pid, value);
      p1strcat(fname, value);
      p1strcat(fname, "/io");

      close(fd);

      fd = open(fname, O_RDONLY | O_EXCL);

      // read 5th line, Read bytes
      for(int j = 0; j < 5; j++)
      p1getline(fd, buf, SIZE);
      idx = p1getword(buf,0,word);
      idx = p1getword(buf,idx,word); // second word
      if((j = p1strchr(word,'\n')) != -1)
      word[j] = '\0';
      p1strcat(output,word);
      p1strcat(output,"\t\t");

      // read 6th Line, write bytes
      p1getline(fd, buf, SIZE);
      idx = p1getword(buf,0,word);
      idx = p1getword(buf,idx,word); // second word
      if((j = p1strchr(word,'\n')) != -1)
      word[j] = '\0';
      p1strcat(output,word);
      p1strcat(output,"\t\t");

      // open /proc/stat for reading
      p1strcpy(fname,"/proc/");
      p1itoa((int)pid, value);
      p1strcat(fname, value);
      p1strcat(fname, "/stat");


      close(fd);

      fd = open(fname, O_RDONLY | O_EXCL);
      p1getline(fd, buf, SIZE);
      // read 14th word, Usertime
      idx = 0;
      for(int j = 0; j < 14; j++)
      idx = p1getword(buf, idx, word);
      if((j = p1strchr(word,'\n')) != -1)
      word[j] = '\0';
      p1strcat(output,word);
      p1strcat(output,"\n");
      // output to stdout
      p1putstr(1,output);

      close(fd);

    }

}

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "p1fxns.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#define SIZE 1024 // size for array storage

volatile int count_children = 0;
volatile int run = 0;
volatile int step = 0;

struct PCB {
    int pid;
    int usr1;
    struct node* next;
};

enum child_code		/* Create an enumeration. */
{
	running =0,
	stopped,
	continued,
	exited
};

void printStatusChildren(pid_t pid[], int nChildren) {
  int status;

  printf("USR1: ");
  for(int i = 0; i < nChildren; i++) {
    waitpid(pid[i],&status,WNOHANG | WCONTINUED | WSTOPPED);
    if(WIFSIGNALED(status))
      if(WTERMSIG(status) == SIGUSR1)
        printf("%d ", pid[i]);
  }
  printf("\n");

  printf("Running: ");
  for(int i = 0; i < nChildren; i++) {
    waitpid(pid[i],&status,WNOHANG | WCONTINUED | WSTOPPED);
    if(WIFEXITED(status) || WIFSTOPPED(status))
      continue;
    printf("%d ", pid[i]);
  }
  printf("\n");

  printf("Stopped: ");
  for(int i = 0; i < nChildren; i++) {
    waitpid(pid[i],&status,WNOHANG | WCONTINUED | WSTOPPED);
    if(WIFSTOPPED(status))
      printf("%d ", pid[i]);
  }
  printf("\n");

  printf("Exited: ");
  for(int i = 0; i < nChildren; i++) {
    waitpid(pid[i],&status,WNOHANG | WCONTINUED | WSTOPPED);
    if(WIFEXITED(status))
      printf("%d ", pid[i]);
  }
  printf("\n");
}
/*	Get the 'status' code of a specific program */

enum child_code get_child_status_code(pid_t pid)
{
	int status;	/* Status to get return value */

	/*
		Call waitpid, passing in the PID, and status address.
		Additionally WNOHANG is specified to return immediately.
		WCONTINUED and WSTOPPED are also specified to indicate
		that if the child was continued or stopped, that status
		should be indicated.

		This must be specified if the child was stopped/continued,
		and you want the result to NOT be exited.
	*/
	int result = waitpid(pid,&status,WNOHANG | WCONTINUED | WSTOPPED);

	if(WIFSTOPPED(status))	/* first check stopped condition */
	{
		printf("%d\tProcess %d is stopped. \r\n",getpid(),pid);
		return stopped;

	}
	else if(WIFCONTINUED(status))	/* Next check continued condition */
	{
		printf("%d\tProcess %d is continued. \r\n",getpid(),pid);
		return continued;
	}
	else if(WIFEXITED(status))	/* child exited by normal exit */
	{
		printf("%d\tProcess %d has exited normally. \r\n",getpid(),pid);
		int es = WEXITSTATUS(status);
       		printf("Exit status was %d\n", es);
		return exited;
	}
	else				/* child is still running. */
	{
		printf("%d\tProcess %d is still running. \r\n",getpid(),pid);
		return running;
	}
}

void SigCONTHandler(int sig) {
    if(sig != SIGCONT) {
        return;
    }
    printf("SIGCONT pid %d\n",getpid());
    fflush(stdout);
}

// step through interval timer
void timestep(int signum) {

  signal (SIGALRM, SIG_IGN); // ignore SIGALRM
  //printf("Alarm signaled handler\n");
  step = (step + 1) % 2;
  signal (SIGALRM, timestep); // restart handler
}

// alarm handler
void alarm_handler(int signum) {
/*
Send a SIGSTOP to the current process
enqueue the current process onto the ready queue
dequeue a process from the ready queue
Send SIGCONT (or SIGUSR1 if this is the first time it is scheduled) to that process
Assign that process's PCB to the current process pointer
*/

}


// check if child process is running
int isChildRunning(pid_t pid) {
	int status;
	(void) waitpid(pid,&status,WNOHANG | WCONTINUED | WSTOPPED);
	if(WIFSTOPPED(status) || WIFCONTINUED(status) || WIFSIGNALED(status) || WIFEXITED(status))
		return 0;
  return 1;
}

// check if child process is continued
int isChildContinued(pid_t pid) {
	int status;
	(void) waitpid(pid,&status,WNOHANG | WCONTINUED | WSTOPPED);
	if(WIFCONTINUED(status))
		return 1;
  return 0;
}

// check if child process has exited
int didChildExit(pid_t pid) {
	int status;
	(void) waitpid(pid,&status,WNOHANG | WCONTINUED | WSTOPPED);
	if(WIFEXITED(status))
		return 1;
  return 0;
}

// check if child process is suspended
int isChildSuspended(pid_t pid) {
	int status;
	(void) waitpid(pid,&status,WNOHANG | WCONTINUED | WSTOPPED);
	if(WIFSTOPPED(status))
		return 1;
  return 0;
}

// counter child processes
void countChildren(int signum) {
  count_children++;
}

// pause or unpause child processes
void runChild(int signum) {
  run = 1;
}

// write string to stdout
int pstdout(char *str) {
    write(STDOUT_FILENO, str, sizeof(char) * p1strlen(str));

    return 0;
}

// fill array with char ch
void clearArray(char buf[], int sz, char ch) {
  for(int i = 0; i < sz; i++) {
    buf[i] = ch;
  }
}

// append word to end of program arguments string buffer
int appendWord(char prog[], int idx, char word[]) {
  int i = 0;
  while(word[i] != '\0')
    prog[idx++] = word[i++];
  prog[idx++] = '\0';
  return idx;
}

// remove any empty arguments (parsing artifacts)
void removeEmptyArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++) {
    if ( args[i] == NULL )
      break;
    if ( p1strlen(args[i]) == 0  )
      args[i] = NULL;
  }
}

// clear list of arguments
void clearArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++)
    args[i] = NULL;
}

int main(int argc, char *argv[] ) {
	pid_t pid[SIZE]; /* TODO: Allow unlimited number of pids */
  int USR[SIZE] = {0}; // usr1 flag
  int EXIT[SIZE] = {0}; // exit flag
  int fd, status;
  char program[SIZE];
  char buf[SIZE];
  char word[SIZE];
  char * args[SIZE]; /* pointer array for program args */
  char message[SIZE];
  char pidno[64];


	// hangle arguments
	if (argc == 1) {

    /* READ FROM STDIN */
    fd = 0;

  } else if (argc == 2) {

    // try to read file
    fd = open(argv[1], O_RDONLY | O_EXCL);
    if(fd < 0) {
      p1strcpy(message,"Error opening ");
      p1strcat(message,argv[1]);
      p1perror(fd,message);
    }

  } else {

    // TOO MANY ARGS!
    p1strcpy(message,"Too many arguments!\n");
    pstdout(message);
    return -1;

  }

  /* LOOP THROUGH PROGRAM LIST */
  int idx_program = 0;
  while( p1getline(fd, buf, SIZE) != 0 ) {

    // init parameters
    clearArgs(args,SIZE);
    clearArray(program, SIZE, '\0');
    int j = 0;
    int cnt = 0;
    int idx = 0;
    int argc = 0;

    // parse program name and arguments
    for (;;) {

      // get next word
      idx = p1getword(buf, idx, word);
      if (idx < 0)
        break;

      // construct array of argument pointers using this word
      args[cnt++] = &program[j];
      j = appendWord(program, j, word);
      argc++;

    }

    // set last argument to NULL
    args[cnt] = NULL;

    // remove newlines for program string
    for(int i = 0; i < SIZE; i++) {
      if(program[i] == '\n')
        program[i] = '\0';
    }

    // remove empty args
    removeEmptyArgs(args, SIZE);

    // ignore SIGUSR1 for now
    signal (SIGUSR1, SIG_IGN);

		// FORK, IF CHILD then EXECVP and EXIT
    if ( (pid[idx_program] = fork()) == -1 )
      perror("fork() error");

    // if this is child process
    // then execute program then exit
    if (pid[idx_program] == 0) {

      signal(SIGCONT, SigCONTHandler);

      sleep(1);

      kill(getppid(),SIGUSR1);

      // wait for SIGUSR1 before execution
      signal (SIGUSR1, runChild);

      p1strcpy(message,"Waiting child with pid ");
      p1itoa((int) getpid(),pidno);
      p1strcat(message,pidno);
      p1strcat(message,"\n");
      pstdout(message);

      // wait until SIGUSR1 signal received
      while(run == 0)
        ;

      // signal that child is executing
      kill(getppid(),SIGUSR1);

			p1strcpy(message,"Executing child with pid ");
      p1itoa((int) getpid(),pidno);
      p1strcat(message,pidno);
      p1strcat(message,"\n");
      pstdout(message);

      // execute
      execvp(args[0],args);

			p1strcpy(message,"Error executing in child with pid ");
      p1itoa((int) getpid(),pidno);
      p1strcat(message,pidno);
      perror(message);
      exit(errno);

    }

    // increment program count
    idx_program++;

  }

  enum child_code code;


  // loop until all children are ready
  count_children = 0;
  signal (SIGUSR1, countChildren);
  while(count_children < idx_program)
    ;

  printf("PARENT SIGNALING: %d\n",getpid());


  //
  // for (int i = 0; i < idx_program; i++)
  //   kill(pid[i],SIGUSR1);
  // for (int i = 0; i < idx_program; i++)
  //   kill(pid[i],SIGSTOP);
  // printStatusChildren(pid,idx_program);
  // sleep(5);
  // for (int i = 0; i < idx_program; i++)
  //   kill(pid[i],SIGCONT);
  // sleep(30);
  // printStatusChildren(pid,idx_program);
  // return 0;
  // for (int i = 0; i < idx_program; i++) {
  //   if(USR[i] == 0) {
  //     kill(pid[i],SIGUSR1);
  //     USR[i] = 1;
  //   }
  //   kill(pid[i],SIGCONT);
  //   sleep(5);
  //   printStatusChildren(pid,idx_program);
  //   sleep(5);
  //   //kill(pid[i],SIGSTOP);
  // }
  // return 0;


  // setup interval timer
  signal(SIGALRM,timestep);
  struct itimerval timer;
  // timer with 250 ms period
  timer.it_value.tv_sec = 0;
  timer.it_value.tv_usec = 750000;
  timer.it_interval.tv_sec = 0;
  timer.it_interval.tv_usec = 750000;
  setitimer (ITIMER_REAL, &timer, 0);

  int i = 0; // current pid
  int exit_count = 0; // count how many children have exited

  printStatusChildren(pid,idx_program);
  // synchronize with timer
  // wait for timer to start interval
  while (step == 1)
    ;


  // round robin scheduling loop
  while (1) {


    printf("Continue pid %d\n",pid[i]);
    if(USR[i] == 0)
      kill(pid[i],SIGUSR1);
    else {
      kill(pid[i],SIGCONT);
      USR[i] = 1;
    }

    // break when all child processes have exited
    exit_count = 0;
    for(int j = 0; j < idx_program; j++) {
      if(EXIT[j] == 1)
        exit_count++;
    }
    printf("exit count %d\n",exit_count);
    if ( exit_count == idx_program )
      break;

    // let child process run for interval
    // wait for timer to change
    while (step == 0)
      ;

    sleep(5);

    kill(pid[i],SIGSTOP);
    printf("Stop pid %d\n",pid[i]);

    // increment pid
    i = (i + 1) % idx_program;

    if(didChildExit(pid[i]))
      EXIT[i] = 1;

    printStatusChildren(pid,idx_program);


  }

  printf("idx_program = %d, exit_count = %d", idx_program, exit_count);

  printf("ALL DONE\n");

  // return from parent
  return 0;
}

/*
 *
 * AUTHOR: Andrew Cvitanovich
 * DUCKID: cvitanov
 * TITLE: CIS 415 Project 1
 *
 * THIS IS MY WORK.
 *
 */

#define _POSIX_SOURCE
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/times.h>
#include "p1fxns.h"
// #define DEBUG 0
#define QUANTUM_LEAP 1000 // limit for quantum usec
#define SIZE 1024 // size for array storage
#define PID_LIMIT 32768 // max number of processes

volatile int count_children = 0;
volatile int run = 0;
volatile int step = 0;
volatile int num_exits = 0;
volatile int EXITS[PID_LIMIT] = {0};
pid_t pids[PID_LIMIT]; /* TODO: Allow unlimited number of pids */

struct ready_queue {
  pid_t Q[PID_LIMIT];
  int tail;
};

struct exit_queue {
  pid_t Q[PID_LIMIT];
  int tail;
};

volatile struct ready_queue RQ;
volatile struct exit_queue EQ;

int idx_program = 0;
int active_processes;

/* SIGCHLD handler. */

static void CHLD_handler() {

  sigset_t signal_set;
  sigemptyset(&signal_set);
  sigaddset(&signal_set, SIGALRM);
  sigprocmask(SIG_BLOCK, &signal_set, NULL);  /* block sig timer signal */

  pid_t pid;
  int status;
  signal(SIGCHLD, CHLD_handler);
  /* Wait for all dead processes.
  * We use a non-blocking call to be sure this signal handler will not
  * block if a child was cleaned up in another part of the program. */
  while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
    if (WIFEXITED(status) || WIFSIGNALED(status)) {
      active_processes--;
      for(int j; j < idx_program; j++) {
        if(pids[j] == pid) {
          EXITS[j] = 1;
          break;
        }
      }
    }
  }

  sigprocmask(SIG_UNBLOCK, &signal_set, NULL);  /* reenable sig timer signal */
}



// check if child process is alive
int isChildAlive(pid_t pid) {
	int status;
	pid_t result = waitpid(pid, &status, WNOHANG);
	if(result == 0)
		return 1;
	return 0;
}

// check if child was signaled
int isChildSignaled(pid_t pid) {
	int status;
	waitpid(pid,&status,WNOHANG | WCONTINUED | WSTOPPED);
	if(WIFSIGNALED(status))
		return 1;
	return 0;
}

// pause or unpause child processes
void runChild() {
  signal (SIGUSR1, SIG_IGN); // reset SIGUSR1
  run = 1;
}

// write string to stdout
int pstdout(char *str) {
    write(STDOUT_FILENO, str, sizeof(char) * p1strlen(str));

    return 0;
}

// fill array with char ch
void clearArray(char buf[], int sz, char ch) {
  for(int i = 0; i < sz; i++) {
    buf[i] = ch;
  }
}

// append word to end of program arguments string buffer
int appendWord(char prog[], int idx, char word[]) {
  int i = 0;
  while(word[i] != '\0')
    prog[idx++] = word[i++];
  prog[idx++] = '\0';
  return idx;
}

// remove any empty arguments (parsing artifacts)
void removeEmptyArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++) {
    if ( args[i] == NULL )
      break;
    if ( p1strlen(args[i]) == 0  )
      args[i] = NULL;
  }
}

// clear list of arguments
void clearArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++)
    args[i] = NULL;
}

// step through interval timer
void timer_handler() {

  sigset_t signal_set;
  sigemptyset(&signal_set);
  sigaddset(&signal_set, SIGCHLD);
  sigprocmask(SIG_BLOCK, &signal_set, NULL);  /* block child exit signal */

  raise(SIGUSR2);

  step++;

  // if(DEBUG) {printf("Timer expired %d times\n",step);}

  sigprocmask(SIG_UNBLOCK, &signal_set, NULL);  /* reenable child exit signal */

}

// dequeue ready queue
pid_t ready_dequeue() {
  if(RQ.tail == 0)
    return RQ.Q[RQ.tail--];
  if(RQ.tail < 0)
    return -1;
  pid_t pid = RQ.Q[0];
  for(int i = 0; i < RQ.tail; i++) {
    RQ.Q[i] = RQ.Q[i+1];
  }
  RQ.tail--;
  return pid;
}

// get the quantum from environment
int getQuantum(void) {
  int val;
  char * s = getenv("USPS_QUANTUM_MSEC");
  (s != NULL) ? (val = p1atoi(s)) : (val = -1);
  return val;
}

// try to set the quantum value with argument
// if the argument is invalid return -1
int tryToSetQuantum(char arg[]) {
  char q[] = "--quantum=";
  int i = 0;
  char val_str[5];
  int val = 0;
  int cnt = 0;

  // check if valid argument
  while(i < 10) {
    if (q[i] != arg[i] || arg[i] == '\0')
      return -1; // not a valid argument for setting quantum
    i++;
  }

  // check if valid value
  while(arg[i] != '\0' && cnt < 4) {
    if ( arg[i] < '0' || arg[i] > '9') {
      return -1; // invalid value for quantum
    } else {
      val_str[cnt++] = arg[i++];
    }
  }
  val = p1atoi(val_str);

  // if(DEBUG) {printf("val = %d\n", val);}

  // return default if value exceeds max quantum limit
  if (val >= QUANTUM_LEAP)
    return getQuantum();

  // return value in argument
  return val;
}

int main(int argc, char *argv[] ) {
  int fd, status, quantum;
  char program[SIZE];
  char buf[SIZE];
  char word[SIZE];
  char * args[SIZE]; /* pointer array for program args */
  char message[SIZE];
  char pidno[64];


	// hangle arguments
	if (argc == 1) {

    // fail if quantum not set
    if ( (quantum = getQuantum()) == -1 )
      return -1;

    /* READ FROM STDIN */
    fd = 0;

  } else if (argc == 2) {

    // try to set quantum
    quantum = tryToSetQuantum(argv[1]);

    if(quantum == -1) {

      // fail if quantum not set
      if ( (quantum = getQuantum()) == -1 )
        return -1;

      // try to read file
      fd = open(argv[1], O_RDONLY | O_EXCL);
      if(fd < 0) {
        p1strcpy(message,"Error opening ");
        p1strcat(message,argv[1]);
        p1perror(fd,message);
      }
    } else {

      // if(DEBUG) {printf("QUANTUM SHOULD BE %d\n", quantum);}
      /* READ FROM STDIN */
      fd = 0;

    }

  } else if (argc == 3) {

    // try to set quantum
    quantum = tryToSetQuantum(argv[1]);


    // bad quantum argument
    if( quantum == -1 )
      return -1;

    // if(DEBUG) {printf("QUANTUM SHOULD BE %d\n", quantum);}

    // try to read file
    fd = open(argv[2], O_RDONLY | O_EXCL);
    if(fd < 0) {
      p1strcpy(message,"Error opening ");
      p1strcat(message,argv[1]);
      p1perror(fd,message);
    }

  } else {

    // TOO MANY ARGS!
    p1strcpy(message,"Too many arguments!\n");
    pstdout(message);
    return -1;

  }

  if(fd < 0)
    return -1;

  /* LOOP THROUGH PROGRAM LIST */
  while( p1getline(fd, buf, SIZE) != 0 ) {

    // init parameters
    clearArgs(args,SIZE);
    clearArray(program, SIZE, '\0');
    int j = 0;
    int cnt = 0;
    int idx = 0;
    int argc = 0;

    // parse program name and arguments
    for (;;) {

      // get next word (ignore newlines)
      idx = p1getword(buf, idx, word);

      if (idx < 0)
        break;

      // construct array of argument pointers using this word
      args[cnt++] = &program[j];
      j = appendWord(program, j, word);
      argc++;

    }

    // set last argument to NULL
    args[cnt] = NULL;

    // remove newlines for program string
    for(int i = 0; i < SIZE; i++) {
      if(program[i] == '\n')
        program[i] = '\0';
    }

    // remove empty args
    removeEmptyArgs(args, SIZE);

    // skip empty programs
    if(args[0] == NULL)
      continue;

    // ignore SIGUSR1 for now
		count_children = 0;
    signal (SIGUSR1, SIG_IGN);


		// FORK, IF CHILD then EXECVP and EXIT
    if ( (pids[idx_program] = fork()) == -1 )
      perror("fork() error");

    // if this is child process
    // then execute program then exit
    if (pids[idx_program] == 0) {
      //
      // if(DEBUG) {
			// 	p1strcpy(message,"Waiting child with pid ");
			// 	p1itoa((int) getpid(),pidno);
			// 	p1strcat(message,pidno);
			// 	p1strcat(message,"\n");
			// 	pstdout(message);
			// }

			/* adapted from https://www.ibm.com/support/knowledgecenter/en/SSB23S_1.1.0.15/gtpc2/cpp_sigsuspend.html */
			sigset_t mask;
			int sig;
			/* initialize the new signal mask */
			sigemptyset(&mask);
			sigaddset(&mask,SIGUSR1);
			// block with mask
			sigprocmask(SIG_BLOCK, &mask, NULL);
			// wait for SIGUSR1
	    sigwait(&mask,&sig);

			// if(DEBUG) {
			// 	p1strcpy(message,"Executing child with pid ");
			// 	p1itoa((int) getpid(),pidno);
			// 	p1strcat(message,pidno);
			// 	p1strcat(message,"\n");
			// 	pstdout(message);
			// }

      // execute
      execvp(args[0],args);

			p1strcpy(message,"Error executing in child with pid ");
      p1itoa((int) getpid(),pidno);
      p1strcat(message,pidno);
      perror(message);
      exit(errno);
    }

    // increment program count
    idx_program++;

  }

  // if(DEBUG) printf("QUANTUM = %d\n", quantum);

  // listening for exiting children and reap with SIGCHLD handler
  if (signal(SIGCHLD, CHLD_handler) == SIG_ERR) {
    return 1;
  } else {
    active_processes = idx_program;
  }


  struct sigaction sa;
  struct itimerval timer;

  memset (&sa, 0, sizeof (sa));
  sa.sa_handler = &timer_handler;
  sigaction (SIGALRM, &sa, NULL);

  timer.it_value.tv_sec = 0;
  timer.it_value.tv_usec = quantum*1000;
  timer.it_interval.tv_sec = 0;
  timer.it_interval.tv_usec = quantum*1000;

  setitimer (ITIMER_REAL, &timer, NULL);


  // prepare ready_queue
  for(int i = 0; i < idx_program; i++) {
    RQ.Q[i] = pids[i];
    RQ.tail = i;
  }


  int exit_flag = 0;
  pid_t current_pid = 0;
  // round robin scheduling loop
  while (active_processes > 0) {
    // wait for timer to change
    sigset_t mask;
    int sig;
    /* initialize the new signal mask */
    sigemptyset(&mask);
    sigaddset(&mask,SIGUSR2);
    // block with mask
    sigprocmask(SIG_BLOCK, &mask, NULL);
    // wait for SIGUSR1
    sigwait(&mask,&sig);

    // check if exited
    for(int j = 0; j < idx_program; j++) {
      if(pids[j] == current_pid) {
        exit_flag = EXITS[j];
        break;
      }
    }

    // not exited continue with remaining steps
    if(exit_flag == 0) {
      // send SIGSTOP to current_pid
      if(current_pid > 0) {
        kill(current_pid,SIGSTOP);
      }
      // if(DEBUG) {printf("STOPPING PROCESS %d\n",current_pid);}

      // enqueue current_pid onto ready queue
      if(current_pid > 0) {
        RQ.tail++;
        RQ.Q[RQ.tail] = current_pid;
      }
    }


    // dequeue a process from the ready queue
    if(RQ.tail >= 0) {
      current_pid = ready_dequeue();
    }

    // send SIGCONT or SIGUSR1
    if(isChildSignaled(current_pid) == 0)
      kill(current_pid,SIGUSR1);
    else {
      kill(current_pid,SIGCONT);
    }
    // if(DEBUG) {printf("EXECUTING PROCESS %d\n",current_pid);}
  }

  // wait for remaining programs to finish
  for(int i = 0; i < idx_program; i++)
    (void) waitpid(pids[i],&status,0);

  exit(0);
}

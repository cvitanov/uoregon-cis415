#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

void timer_handler(int signal) {
 static int count = 0;
 printf ("timer expired %d times\n", ++count);
}

int main () {

  signal(SIGALRM,timer_handler);

  struct itimerval timer;

  // timer with 250 ms period
  timer.it_value.tv_sec = 0;
  timer.it_value.tv_usec = 250000;
  timer.it_interval.tv_sec = 0;
  timer.it_interval.tv_usec = 250000;

  /* Start a virtual timer. It counts down whenever this process is
   executing. */
  setitimer (ITIMER_REAL, &timer, 0);

  /* Do busy work. */
  while (1);
}

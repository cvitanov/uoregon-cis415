#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include "p1fxns.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#define PRINT 1
#define SIZE 1024 // size for array storage
#define QUANTUM_LEAP 2500 // limit for quantum usec

// fill array with char ch
void clearArray(char buf[], int sz, char ch) {
  for(int i = 0; i < sz; i++) {
    buf[i] = ch;
  }
}

// append word to end of program arguments string buffer
int appendWord(char prog[], int idx, char word[]) {
  int i = 0;
  while(word[i] != '\0')
    prog[idx++] = word[i++];
  prog[idx++] = '\0';
  return idx;
}

// remove any empty arguments (parsing artifacts)
void removeEmptyArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++) {
    if ( args[i] == NULL )
      break;
    if ( p1strlen(args[i]) == 0  )
      args[i] = NULL;
  }
}

// print arguments for a program string
void printArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++) {
    if (args[i] != NULL)
      if(PRINT) printf("%s len: %d\n",args[i], p1strlen(args[i]));
  }
}

// clear list of arguments
void clearArgs(char * args[], int sz) {
  for(int i = 0; i < sz; i++)
    args[i] = NULL;
}

// get the quantum from environment
int getQuantum(void) {
  int val;
  const char * s = getenv("USPS_QUANTUM_MSEC");
  (s != NULL) ? (val = p1atoi(s)) : (val = -1);
  return val;
}

// try to set the quantum value with argument
// if the argument is invalid return -1
int tryToSetQuantum(char arg[]) {
  char q[] = "--quantum=";
  int i = 0;
  char val_str[5];
  int val = 0;
  int cnt = 0;
  int success;

  // check if valid argument
  while(i < 10) {
    if (q[i] != arg[i] || arg[i] == '\0')
      {if (PRINT) printf("Failure!\n"); return -1;} // not a valid argument for setting quantum
    i++;
  }

  // check if valid value
  while(arg[i] != '\0' && cnt < 4) {
    if ( arg[i] < '0' || arg[i] > '9') {
      {if (PRINT) printf("Failure!\n"); return -1;} // invalid value for quantum
    } else {
      val_str[cnt++] = arg[i++];
    }
  }
  val = p1atoi(val_str);

  // fail if value exceeds max quantum limit
  if (val >= QUANTUM_LEAP)
    {if (PRINT) printf("Failure!\n"); return -1;}

  // set environment variable
  success = setenv("USPS_QUANTUM_MSEC", val_str, 1);
  if(success == 0)
    return val;
  {if (PRINT) printf("Failure!\n"); return -1;}
}

/* launch each program with fork(), execvp(), etc. */
/* NOTE: Need to use _exit() or exit() to avoid fork bomb? */

int main(int argc, char *argv[] ) {
  pid_t pid[SIZE]; /* TODO: Allow unlimited number of pids */
  int fd, sz, rval, quantum, status, result;
  int count, idx; /* for parsing program string */
  char * path = argv[1];
  char program[SIZE];
  char buf[SIZE];
  char word[SIZE];
  char * args[SIZE]; /* pointer array for program args */

  // /* Check file existence. */
  // rval = access (path, F_OK);
  // if (rval == 0)
  //  if (PRINT) printf ("%s exists\n", path);
  // else {
  //  if (errno == ENOENT)
  //   if (PRINT) printf ("%s does not exist\n", path);
  //  else if (errno == EACCES)
  //   if (PRINT) printf ("%s is not accessible\n", path);
  //  return 0;
  // }

  // /* Check read access. */
  // rval = access (path, R_OK);
  // if (rval == 0)
  //  if (PRINT) printf ("%s is readable\n", path);
  // else
  //  if (PRINT) printf ("%s is not readable (access denied)\n", path);

  /* Read workload from stdin or file */

  if (PRINT) printf("CURRENT QUANTUM: %d\n",getQuantum());

  if (argc == 1) {

    if (PRINT) printf("Reading from stdin with default quantum.\n");

    // fail if quantum not set
    if ( getQuantum() == -1 )
      {if (PRINT) printf("Failure!\n"); return -1;}

    /* READ FROM STDIN */
    fd = 0;

  } else if (argc == 2) {

    // try to set quantum
    quantum = tryToSetQuantum(argv[1]);

    if(quantum == -1) {

      // fail if quantum not set
      if ( getQuantum() == -1 )
        {if (PRINT) printf("Failure!\n"); return -1;}

      if (PRINT) printf("Reading from file with default quantum.\n");

      // try to read file
      fd = open(argv[1], O_RDONLY | O_EXCL);

    } else {

      if (PRINT) printf("Reading from stdin with quantum = %d\n", getQuantum());

      /* READ FROM STDIN */
      fd = 0;

    }

  } else if (argc == 3) {

    // try to set quantum
    quantum = tryToSetQuantum(argv[1]);

    // bad quantum argument
    if( quantum == -1 )
      {if (PRINT) printf("Failure!\n"); return -1;}

    if (PRINT) printf("Reading from file with quantum = %d\n", getQuantum());

    // try to read file
    fd = open(argv[2], O_RDONLY | O_EXCL);

  } else {

    // TOO MANY ARGS!
    {if (PRINT) printf("Failure!\n"); return -1;}

  }

  // bad file descriptor
  if (fd < 0)
    {if (PRINT) printf("Failure!\n"); return -1;}

  /* read */

  /* TODO: use p1getword to assign pointers of argv[SIZE] to program name and arguments
  NOTE: argv[0] is program name, must have NULL at pointer after the last arg */

  /* LOOP THROUGH PROGRAM LIST */
  int idx_program = 0;
  while( p1getline(fd, buf, SIZE) != 0 ) {
    if (PRINT) printf("PARSING: %s\n",buf);

    // PARSE
    // init parameters
    clearArgs(args,SIZE);
    clearArray(program, SIZE, '\0');
    int j = 0;
    int cnt = 0;
    int idx = 0;
    int argc = 0;
    // parse program name and arguments
    for (;;) {

      // get next word
      idx = p1getword(buf, idx, word);
      if (idx < 0)
        break;

      if (PRINT) printf("WORD #%d: %s\n",cnt,word);
      if (PRINT) printf("IDX: %d\n", idx);
      // construct array of argument pointers using this word
      args[cnt++] = &program[j];
      j = appendWord(program, j, word);
      argc++;

    }

    // set last argument to NULL
    args[cnt] = NULL;

    // remove newlines for program string
    for(int i = 0; i < SIZE; i++) {
      if(program[i] == '\n')
        program[i] = '\0';
    }

    // remove empty args
    removeEmptyArgs(args, SIZE);

    // print args
    if (PRINT) printf("PRINTING ARGS (count = %d): program #%d\n",argc, idx_program+1);
    //printArgs(args,SIZE);


    // FORK, IF CHILD EXECVP and EXIT
    pid[idx_program] = fork();
    // if this is child process
    // then execute program then exit
    if (pid[idx_program] == 0) {
      execvp(args[0],args);
      exit(0);
    }

    // increment program count
    idx_program++;
    // pid[i] = fork();
    // if (PRINT) printf("BUF: %s\n",buf);

    // // set program name
    // count = 0;
    // args[count++] = &program[0];
    // for(;;) {
    //   idx = p1getword(buf, idx, &program[idx]);
    //   args[count++] = &program[idx];
    //   program[idx++] = ':';
    //   if (PRINT) printf("idx=%d, program=%s\n",idx,program);
    //   if (PRINT) printf("end\n");
    //   if(0 >= idx) break;
    // }
    // if (PRINT) printf("HERE\nProgram: %s\n",program);
    // if (PRINT) printf("PRINT ARGS: \n");
    // printArgs(args,SIZE);
    // clearArray(buf, SIZE, '\0');
  }

  if (PRINT) printf("%d programs!\n",idx_program);
  int done_count = 0;
  for(int i = 0; i < idx_program; i++) {
    result = waitpid(pid[i],&status,0);
    if (PRINT) printf("DONE: pid %d status %d result %d\n", pid[i], status);
    // if (WIFEXITED(status)) {
    //     printf("%d", WEXITSTATUS(status));
    // }
    done_count++;
  }
  if (PRINT) printf("%d programs exited!\n",done_count);
  return 0;

  // if (PRINT) printf("fd = %d\n", fd);
  // sz = read(fd, c, 10);
  // if (PRINT) printf("fd = %d\n", fd);
  //
  // if (PRINT) printf( "called read(%d, c, 10).  returned that"
  //         " %d bytes  were read.\n", 3, sz);
  // c[sz] = '\0';
  // if (PRINT) printf("Those bytes are as follows: %s\n", c);
  //
  // /* close workload file */
  // if (close(fd) < 0) {
  //   perror("c1");
  //   exit(1);
  // }
}

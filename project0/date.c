/*
 *
 * AUTHOR: Andrew Cvitanovich
 * DUCKID: cvitanov
 * TITLE: CIS 415 Project 0
 *
 * THIS IS MY WORK.
 *
 */

#include "date.h"
#include <stdio.h>
#include <stdlib.h>

/* data for a date object */
typedef struct d_data {
  int year;
  int month;
  int day;
} DData;

/* declare functions used in Date structure */
static const Date * d_duplicate(const Date *d);
static int d_compare(const Date *date1, const Date *date2);
static void d_destroy(const Date *d);


/* template for Date object construction */
static Date template = {NULL, d_duplicate, d_compare, d_destroy};

/* copy constructor duplicate */
static const Date * d_duplicate(const Date *d) {
  DData *dData = (DData *)(d->self);
  Date *dtptr = (Date *)malloc(sizeof(Date));
  if(dtptr != NULL) {
    DData *dDataNew = (DData *)malloc(sizeof(DData));
    if(dData != NULL) {
      *dtptr = template;
      /* process year */
      dDataNew->year = dData->year;
      /* process month */
      dDataNew->month = dData->month;
      /* process day */
      dDataNew->day = dData->day;
      /* assign data to Date struct */
      dtptr->self = dDataNew;
    } else {
      free(dtptr);
      dtptr = NULL;
    }
  }
  return dtptr;
}

/* compare */
static int d_compare(const Date *date1, const Date *date2) {
  DData *dData1 = (DData *)(date1->self);
  DData *dData2 = (DData *)(date2->self);
  int comp = (dData1->year) - (dData2->year);
  if(comp != 0)
    return comp;
  comp = (dData1->month) - (dData2->month);
  if(comp != 0)
    return comp;
  return (dData1->day) - (dData2->day);
}

/* destroy */
static void d_destroy(const Date *d) {
  DData *dData = (DData *)(d->self);
  free(dData);
  dData = NULL;
  free((void *)d);
  d = NULL;
}

/* Date_create definition */
const Date * Date_create(char *datestr) {
  Date *dtptr = (Date *)malloc(sizeof(Date));
  if(dtptr != NULL) {
    DData *dData = (DData *)malloc(sizeof(DData));
    if(dData != NULL) {
      *dtptr = template;

      int day, month, year;
      char delim1, delim2;
      sscanf(datestr, "%d%c%d%c%d", &day, &delim1, &month, &delim2, &year);
      dData->day = day;
      dData->month = month;
      dData->year = year;

      // /* process day */
      // dData->day = 0;
      // dData->day  += (datestr[0] - '0') * 10;
      // dData->day  += (datestr[1] - '0');
      //
      // /* process month */
      // dData->month = 0;
      // dData->month  += (datestr[3] - '0') * 10;
      // dData->month  += (datestr[4] - '0');
      //
      // /* process year */
      // dData->year = 0;
      // dData->year += (datestr[6] - '0') * 1000;
      // dData->year += (datestr[7] - '0') * 100;
      // dData->year += (datestr[8] - '0') * 10;
      // dData->year += (datestr[9] - '0');



      /* assign data to Date struct */
      dtptr->self = dData;
    } else {
      free(dtptr);
      dtptr = NULL;
    }
  }
  return dtptr;
}

/*
 *
 * AUTHOR: Andrew Cvitanovich
 * DUCKID: cvitanov
 * TITLE: CIS 415 Project 0
 *
 * THIS IS MY WORK.
 * 
 */
#include "iterator.h"
#include "tldmap.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

struct tldnode {
  char * theTLD;
  long value;
  TLDNode *left;
  TLDNode *right;
};

/* declare methods */
static void tld_destroy(const TLDMap *tld);
static int tld_insert(const TLDMap *tld, char *theTLD, long v);
static int tld_reassign(const TLDMap *tld, char *theTLD, long v);
static int tld_lookup(const TLDMap *tld, char *theTLD, long *v);
static const Iterator * tld_itCreate(const TLDMap *tld);

/* declare bst helper functions */
TLDNode * searchBST(TLDNode * root, char * key);
TLDNode * insertBST(TLDNode * node, char * theTLD);
void deleteBST(TLDNode * node);
long sizeBST(TLDNode * root);

/* template for TLDNode object construction */
static TLDMap template = {NULL, tld_destroy, tld_insert, tld_reassign, tld_lookup, tld_itCreate};


/* BST HELPER FUNCTIONS */

/* search BST */
TLDNode * searchBST(TLDNode * root, char * key) {
    if (root == NULL)
      return NULL;
    if (root->theTLD == NULL)
      return NULL;
    if (strcmp(root->theTLD, key) == 0)
      return root;
    else if (strcmp(root->theTLD, key) > 0)
      return searchBST(root->left, key);
    else
      return searchBST(root->right, key);
}

/* insert key into BST */
TLDNode * insertBST(TLDNode * node, char * theTLD) {
  if ( node == NULL ) {
    /* create new node */
    node =  (TLDNode *)malloc(sizeof(TLDNode));
    node->theTLD = malloc(strlen(theTLD) + 1);
    strcpy(node->theTLD,theTLD);
    node->value = 1;
    node->left = node->right = NULL;

    /* return the node */
    return node;
  }
  if ( strcmp(node->theTLD, theTLD) > 0 )
    node->left = insertBST(node->left, theTLD);
  else if ( strcmp(node->theTLD, theTLD) < 0 )
    node->right = insertBST(node->right, theTLD);
  return node;
}

/* delete the BST */
void deleteBST(TLDNode * node) {
    if (node == NULL) return;
    deleteBST(node->left);
    deleteBST(node->right);
    free(node->theTLD);
    free(node);
    node = NULL;
}

/* print BST */
void printBST(TLDNode * node) {
  if (node == NULL) return;
  if(node->left != NULL)
    printBST(node->left);
  //printf("Node: %s, value = %d, %p\n",node->theTLD, (int)node->value, (void *)node);
  if(node->right != NULL)
    printBST(node->right);
}

/* get BST size */
long sizeBST(TLDNode * root) {
  if(root == NULL || root->value == 0)
    return 0;
  else
    return sizeBST(root->left) + 1 + sizeBST(root->right);
}

static void tld_destroy(const TLDMap *tld) {
  TLDNode * root = (TLDNode *)(tld->self);
  /* delete the BST */
  deleteBST(root);
  /* free map resource */
  free((void *)tld);
  /* nullify pointer */
  tld = NULL;
}

static int tld_insert(const TLDMap *tld, char *theTLD, long v) {
  if( !tld_lookup(tld, theTLD, &v) ) {
    TLDNode *root = (TLDNode *)(tld->self);

    /* lowercase */
    char * p = theTLD;
    for ( ; *p; ++p) *p = tolower(*p);
    TLDNode * n = NULL;

    if(root->theTLD==NULL) {
      /* initialize root of BST with first node */
      root->theTLD = malloc(strlen(theTLD) + 1);
      strcpy(root->theTLD,theTLD);
      root->value = 1;
      root->left = root->right = NULL;
      n = root;
    } else {
      /* insert into existing BST */
      n = insertBST(root, theTLD);
      printBST(root);
    }

    if(n == NULL)
      return 0;

    return 1;

  } else { return 0; }
}

static int tld_reassign(const TLDMap *tld, char *theTLD, long v) {
  /* lowercase */
  char * p = theTLD;
  for ( ; *p; ++p) *p = tolower(*p);

  /* find matching node */
  TLDNode * n = NULL;
  TLDNode * root = (TLDNode *)(tld->self);
  n = searchBST(root, theTLD);
  /* if no such key return 0 */
  if(n==NULL)
    return 0;
  /* reassign value */
  n->value = (int)v;
  return 1;
}

static int tld_lookup(const TLDMap *tld, char *theTLD, long *v) {

  /* lowercase */
  char * p = theTLD;
  for ( ; *p; ++p) *p = tolower(*p);

  /* find matching node */
  TLDNode * n = NULL;
  TLDNode * root = (TLDNode *)(tld->self);
  n = searchBST(root, theTLD);
  int ret = 1;
  if(n==NULL)
    ret = 0;
  else
    *v = n->value;
  return ret;
}


static void inOrderNodesList( TLDNode *node, void ** list, int index) {
  if (node == NULL) return;
  inOrderNodesList(node->left, list, index);
  /* add node to list */
  /* find end of list and append */
  while(list[index] != NULL)
    index++;
  list[index] = (void *)node;
  inOrderNodesList(node->right, list, index);
}


const Iterator * tld_itCreate(const TLDMap *tld) {
  TLDNode *root = (TLDNode *)(tld->self);
  const Iterator *it = NULL;
  int i;

  /* get tree size */
  long size = sizeBST(root);

  if(size > 0L) {
    /* malloc heap space for the array of node pointers */
    size_t nbytes = size * sizeof(void *);
    void **tmp = (void **)malloc(nbytes);
    for(i = 0; i < size; i++)
	tmp[i] = NULL;
    inOrderNodesList(root, tmp, 0); /* create pointer array of nodes */
    /* create iterator */
    it = Iterator_create(size, tmp);
    if (it == NULL)
      free(tmp);
  }
  return it;
}


/* create empty TLDMap */
const TLDMap *TLDMap_create(void) {
  TLDMap * mapPtr = (TLDMap *)malloc(sizeof(TLDMap));
  if(mapPtr != NULL) {
    *mapPtr = template;
    TLDNode * root = (TLDNode *)malloc(sizeof(TLDNode));
    root->theTLD = NULL;
    root->value = 0;
    mapPtr->self = root;
  } else {
    free(mapPtr);
    mapPtr = NULL;
  }
  return mapPtr;
}

/* returns key of tld node */
char * TLDNode_tldname(TLDNode *node) {
  return node->theTLD;
}

/* returns value of tld node */
long TLDNode_count(TLDNode *node) {
  long retVal = node->value;
  return retVal;
}

NAME: Andrew Cvitanovich
DUCKID: cvitanov
DATE: April 15, 2018
TITLE: CIS 415 PROJECT 0

I was able to successfully implement the project without memory leaks (checked with valgrind). I made extensive use of gdb and git to keep track of bugs in my source code. My final implementation uses a binary search tree to store top level domain data.
